//Constructor

function Book(title, author, year) {
    this.title = title;
    this.author = author;
    this.year = year;


    this.getSummary = function(){
        return `${this.title} was written by ${this.author} in ${this.year}`;
    }
}


//Instentiate an object

const book1 = new Book('Book One', 'Mamun', '2019');
const book2 = new Book('Book Two', 'Dumb', '2019');

console.log(book1.getSummary());