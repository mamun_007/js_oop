class Book
{
    constructor (title, author, year) {
        this.title = title;
        this.author = author;
        this.year = year;
    }

    getSummary() {
        return `${this.title} was written by ${this.author} in ${this.year}`;
    }
    

    getAge(){
        const years = new Date().getFullYear - this.year;
        return `${this.title} was written by ${this.author} ${years} ago.`;
    }

    revise(newYear){
       this.year = newYear;
       this.revised = true; 
    }

    static topBookStore (){
        return 'BookEnds';
    }
}

//Instantiate Object

const book1 = new Book('Book One', 'John', '2013');
console.log(book1.getSummary()); //logs the first function
console.log(book1.getAge()); //logs the 2nd function
book1.revise('1996')
console.log(book1.year); //logs the 3rd function

console.log(Book.topBookStore()); //logs the 4th function without instantiate


