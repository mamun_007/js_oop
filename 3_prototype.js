//Constructor

function Book(title, author, year) {
    this.title = title;
    this.author = author;
    this.year = year;
}


//Prototype of an object funstion

Book.prototype.getSummary = function () {
    return `${this.title} was written by ${this.author} in ${this.year}`;
}

//Protoype two

Book.prototype.getAge = function(){
    const years = new Date().getFullYear()-this.year;
    return `${this.title} was written by ${this.author} ${years} years ago.`;;
}



//Instentiate an object

const book1 = new Book('Book One', 'Mamun', '2015');
const book2 = new Book('Book Two', 'Dumb', '2019');

console.log(book1.getSummary());
console.log(book1.getAge());