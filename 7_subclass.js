class Book
{
    constructor (title, author, year) {
        this.title = title;
        this.author = author;
        this.year = year;
    }

    getSummary() {
        return `${this.title} was written by ${this.author} in ${this.year}`;
    }
}    

//magazine Subclass

class Magazine extends Book {
    constructor(title, author, year, month){
        super(title, author, year);
        this.month = month;
    }
}

//Instansiate Subclass
const mag1 = new Magazine('Magazine One', 'Mamun', '2018', 'Mar');
console.log(mag1); //Access subclass and class together
console.log(mag1.getSummary()); //Access method from super class through sub class