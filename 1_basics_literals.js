//Object Literals
const book1 = {
    title: 'Book One',
    author: 'Dumb',
    year: '2019',
    getSummary: function () {
        return `${this.title} was written by ${this.author} in ${this.year}`;
    }
};

console.log(book1.getSummary());


const book2 ={
    title: 'Book Two',
    author: 'Asshole',
    year: '2018',
    getSummary: function() {
        return `${this.title} was written by ${this.author} in ${this
        .year}`
    }
};

console.log(Object.values(book2));   //reaturns as array.