//Constructor

function Book(title, author, year) {
    this.title = title;
    this.author = author;
    this.year = year;
}


//Prototype of an object funstion

Book.prototype.getSummary = function () {
    return `${this.title} was written by ${this.author} in ${this.year}`;
}

// Magazine constructor
function Magazine(title, author, year, month) {
    Book.call(this, title, author, year); //Inherite constructor

    this.month = month;
}

//Inherit Prototype
Magazine.prototype = Object.create(Book.prototype);


const magazine1 = new Magazine ('Mag One', 'John', '2018', 'Mar');
console.log(magazine1);

